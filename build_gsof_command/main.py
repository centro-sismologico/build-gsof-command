from pydantic.dataclasses import dataclass
from pydantic import Field
from pathlib import Path
import tomllib
import typer
from typing import Optional, List, Set
import httpx
import ujson as json
from rich import print


def comando(data):
    code = data["code"]
    host = data["protocol_host"]
    port = data["port"]
    return f"gsof --code {code} --host {host} --port {port}"


@dataclass
class Settings:
    server: str
    url: str
    names: Path
    network: List[str]
    names_list: Set = Field(default_factory=set)
    data: None | List = None

    def __post_init__(self):
        self.data = self.get_data()
        self.read_names()

    def __repr__(self):
        return f"Settings({self.server}, {self.url}, {self.names}, {self.network})"

    def get_data(self) -> List | None:
        result = httpx.get(self.url)
        if result.status_code == 200:
            return {d["code"]: d for d in json.loads(result.content)}
        else:
            return None

    def commands(self) -> List[str]:
        comandos = []
        for key in set(self.data) & self.names_list:
            network = self.data[key]["network"]
            # if network in self.network:
            c = comando(self.data[key])
            comandos.append(c)
            # else:
            #   print(
            #        f"Estación {self.data[key]} no está en red {self.network}")
        return comandos

    def add_name(self, name: str):
        self.names_list.add(name)

    def read_names(self):
        if self.names.exists():
            for n in self.names.read_text().split("\n"):
                self.add_name(n)
        else:
            print(f"Archivo {self.names} no existe")


def read_settings(path: Path) -> Settings | None:
    if path.exists():
        t = path.read_text()
        data = tomllib.loads(t)
        settings = Settings(**data["source"])
        return settings
    else:
        return None


app = typer.Typer()


@app.command("gsof_command")
def run(path: Path):
    print("Path", path)
    match read_settings(path):
        case None:
            print("No hay path a settings")
        case Settings() as settings:
            print(settings)
            for c in settings.commands():
                print(c)
        case _:
            print("Otro caso")


def main():
    print("Running main")
    app()


if __name__ == "__main__":
    app()
