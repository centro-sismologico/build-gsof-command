Install poetry:

https://python-poetry.org/

Para instalar (using poetry):

``` bash
poetry install
```

Running with 'poetry' manager.

``` bash
poetry run gsof_command settings/data.toml 
```

To other list, define the list of stations:

- on a file 'names.txt' 
- copy data.toml and link to 'names.txt' 

Then, run the command again.

Then, run every command to check communication with stations.
